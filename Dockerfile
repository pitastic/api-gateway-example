FROM debian:jessie-slim

# install awscli and jq
RUN apt-get update \
 && apt-get install -y python-pip python-dev groff-base jq \
 && pip install --upgrade awscli \
 && apt-get remove -y python-pip python-dev \
 && rm -rf /var/lib/apt/lists/*
