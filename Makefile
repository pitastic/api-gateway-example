####
#
# makefile to deploy an example aws lambda
#
####

#
# VARIABLES
#

# name of the cloudformation stack to create
STACK_NAME?=pitastic-api-gateway
# domain name for the custom domain name
DOMAIN_NAME?=pitastic.ch
# subdomain name for the api service
SUBDOMAIN_NAME?=api-test

# general shortcuts
DOCKER=docker run --rm -ti --env-file ${CURDIR}/.env -v ${CURDIR}:/data -w /data
BASH=${DOCKER} --entrypoint=/bin/bash piaws -c
JQ=${DOCKER} --entrypoint=/usr/bin/jq piaws

#
# TARGETS
#

# build docker image
docker-image:
	docker build -t piaws .

# deploy cloudformation stack for the cloudfront certificate
cloudformation-cert:
	@echo "create ssl certificate for api.pitastic.ch"
	@${BASH} "aws --region=us-east-1 cloudformation deploy \
		--stack-name ${STACK_NAME}-cert --template-file Cloudformation.certificate.yaml \
		--no-fail-on-empty-changeset"

# initialize the aws api gateway
cloudformation-api:
	@echo "get the certificate outputs"
	${BASH} "aws --region=us-east-1 cloudformation describe-stacks --stack-name ${STACK_NAME}-cert > .certificate.stack"
	@echo "retrieve the certificate arn"
	${JQ} -r '.Stacks[0].Outputs[] | select (.OutputKey == "Certificate") | .OutputValue' .certificate.stack > .certificate.arn

	@echo "execute cloudformation deployment"
	@${BASH} "aws cloudformation deploy \
		--stack-name ${STACK_NAME} --template-file Cloudformation.api.yaml \
		--capabilities CAPABILITY_IAM \
		--no-fail-on-empty-changeset \
		--parameter-overrides \
		    Certificate=$$(cat .certificate.arn) \
		    DomainName=${DOMAIN_NAME} \
		    SubdomainName=${SUBDOMAIN_NAME}"
